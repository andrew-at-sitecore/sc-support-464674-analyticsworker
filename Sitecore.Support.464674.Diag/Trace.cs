﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sitecore.Support
{
  public class Trace
  {
    public static void Info(string message)
    {
      Sitecore.Diagnostics.Log.Info(Contextualize(message), new object());
    }

    private static string Contextualize(string message)
    {
      return String.Format("[DIAG#464674]: {0}", message);
    }
  }

}
