﻿using Sitecore.StringExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sitecore.Support.Analytics.Core
{
  class BackgroundService: Sitecore.Analytics.Core.BackgroundService
  {

    public BackgroundService(string agentName) : base(agentName) { }

    public override void ExecuteAgent()
    {
      Trace.Info(" ~~~ Executing agent [Interval: {0}sec; IsRunning={1}; MaxThreads={2}]".FormatWith(this.Interval.TotalSeconds, this.IsRunning, this.MaxThreads));
      base.ExecuteAgent();
      Trace.Info(" ~~~ Done executing agent");
    }
  }
}
