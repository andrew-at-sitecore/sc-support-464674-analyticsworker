﻿namespace Sitecore.Analytics.Diagnostics.PerformanceCounters
{
  using Sitecore.Diagnostics.PerformanceCounters;
  using System;
  using System.Diagnostics;

  internal class OperationStopwatch : IDisposable
  {
    private readonly AverageCounter counter;
    private readonly long precision;
    private readonly Stopwatch stopwatch;

    public OperationStopwatch(AverageCounter counter, long precision)
    {
      this.counter = counter;
      this.precision = precision;
      if (counter != null)
      {
        this.stopwatch = new Stopwatch();
        this.stopwatch.Start();
      }
    }

    void IDisposable.Dispose()
    {
      if (this.stopwatch != null)
      {
        this.stopwatch.Stop();
        if (this.counter != null)
        {
          long amount = (this.stopwatch.ElapsedTicks * this.precision) / Stopwatch.Frequency;
          this.counter.AddMeasurement(amount, 1L);
        }
      }
    }
  }
}
