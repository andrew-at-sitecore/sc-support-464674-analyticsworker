﻿namespace Sitecore.Support.Analytics.Aggregation
{
  using Sitecore.Analytics.Aggregation;
  using Sitecore.Analytics.Aggregation.Data;
  using Sitecore.Analytics.Aggregation.Data.Processing;
  using Sitecore.Analytics.Configuration;
  using Sitecore.Analytics.Core;
  using Sitecore.Analytics.Diagnostics.PerformanceCounters;
  using Sitecore.Analytics.Model;
  using Sitecore.Analytics.Processing;
  using Sitecore.Analytics.Processing.ProcessingPool;
  using Sitecore.Diagnostics;
  using System;
  using System.Globalization;
  using System.Runtime.CompilerServices;

  public class InteractionBatchAggregationAgent : IAgent
  {
    private const int DEFAULT_BATCH_SIZE = 0x40;
    private int maximumBatchSize = 0x40;

    public void Execute()
    {
      Trace.Info(" > InteractionBatchAggregationAgent::Execute [START]");
      Assert.Required(this.Context, "The InteractionBatchAggregationAgent.Context property has to be set.");
      Assert.Required(this.Context.Pool, "The InteractionBatchAggregationAgent.Context.Pool property has to be set.");
      Assert.Required(this.Context.Source, "The InteractionBatchAggregationAgent.Context.Source property has to be set.");
      Assert.Required(this.Aggregator, "The InteractionBatchAggregationAgent.Aggregator property has to be set.");
      if (!AnalyticsSettings.Enabled)
      {
        Type type = base.GetType();
        Log.Info(string.Format(CultureInfo.InvariantCulture, "Agent '{0}' not executed as Analytics is disabled.", new object[] { type.Name }), this);
      }
      else
      {
        bool flag;
        this.Aggregator.Context = this.Context;
        this.Aggregator.DateTimeStrategy = this.DateTimeStrategy;
        this.Aggregator.CounterAverageAggregationTime = AnalyticsCount.AggregationAverageAggregationPipelineTimems;
        this.Aggregator.CounterAverageWriteTime = AnalyticsCount.AggregationAverageWriteTimems;
        this.Aggregator.CounterAverageWriteTimePrimary = AnalyticsCount.AggregationAverageWriteTimePrimaryms;
        this.Aggregator.CounterAverageWriteTimeSecondary = AnalyticsCount.AggregationAverageWriteTimeSecondaryLivems;
        this.Aggregator.CounterAverageBatchSize = AnalyticsCount.AggregationAverageBatchSizeLive;
        IWorkItemMapper<InteractionKey, ProcessingPoolItem> workItemMapper = new ProcessingPoolItemMapper();
        ProcessingPoolScheduler<InteractionKey> scheduler = new ProcessingPoolScheduler<InteractionKey>(this.Context.Pool, workItemMapper, this.MaximumBatchSize);
        do
        {
          ItemBatch<InteractionKey> batch;
          using (new OperationStopwatch(AnalyticsCount.AggregationAverageCheckOutTimeLivems, 0x3e8L))
          {
            flag = scheduler.TryGetNext(out batch);
          }
          if (flag)
          {
            try
            {
              this.Aggregator.Aggregate(batch);
              using (new OperationStopwatch(AnalyticsCount.AggregationAverageCheckInTimeLivems, 0x3e8L))
              {
                scheduler.MarkProcessed(batch);
              }
            }
            catch (Exception exception)
            {
              Log.Error("Error during aggregation.", exception, this);
            }
            int num = batch.Items.Count - batch.PostponedItems.Count;
            int count = batch.PostponedItems.Count;
            AnalyticsCount.AggregationLiveInteractionsProcessed.Increment((long)num);
            AnalyticsCount.AggregationTotalInteractionsProcessed.Increment((long)num);
            AnalyticsCount.AggregationLiveAggregationErrors.Increment((long)count);
            AnalyticsCount.AggregationTotalAggregationErrors.Increment((long)count);
            if (count > 0)
            {
              AnalyticsCount.AggregationNumberofBatchesContainingFailingItemsLive.Increment(1L);
            }
          }
        }
        while (flag);
      }
      Trace.Info(" > InteractionBatchAggregationAgent::Execute [DONE]");

    }

    public InteractionBatchAggregatorBase Aggregator { get; set; }

    public AggregationContext Context { get; set; }

    public IDateTimePrecisionStrategy DateTimeStrategy { get; set; }

    public int MaximumBatchSize
    {
      get
      {
        return this.maximumBatchSize;
      }
      set
      {
        Assert.ArgumentCondition(value >= 0, "value", "The specified batch size is not valid.");
        this.maximumBatchSize = (value != 0) ? value : 0x40;
      }
    }
  }
}
