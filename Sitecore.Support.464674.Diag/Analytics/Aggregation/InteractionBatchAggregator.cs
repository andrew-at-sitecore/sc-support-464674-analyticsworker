﻿using Sitecore.Analytics.Aggregation.Data.DataAccess;
using System.Collections.ObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Diagnostics;
using Sitecore.Diagnostics.PerformanceCounters;
using Sitecore.Analytics.Diagnostics.PerformanceCounters;
using System.Threading;
using Sitecore.Analytics.Aggregation.Data.Model;
using Sitecore.Analytics.Model;
using Sitecore.StringExtensions;

namespace Sitecore.Support.Analytics.Aggregation
{
  class InteractionBatchAggregator: Sitecore.Analytics.Aggregation.InteractionBatchAggregator
  {
    private static int aggregators;

    private class ReportingStorageItem : IReportingStorageItem
    {
      private readonly IAggregationDataSet data;
      private readonly byte[] id;
      private readonly InteractionKey key;

      internal ReportingStorageItem(byte[] id, IAggregationDataSet data, InteractionKey key)
      {
        this.id = id;
        this.data = data;
        this.key = key;
      }

      public IAggregationDataSet Data
      {
        get
        {
          return this.data;
        }
      }

      public byte[] Id
      {
        get
        {
          return this.id;
        }
      }

      internal InteractionKey Item
      {
        get
        {
          return this.key;
        }
      }
    }

    public override void Aggregate(Sitecore.Analytics.Core.ItemBatch<Sitecore.Analytics.Model.InteractionKey> batch)
    {
      Trace.Info("ENTER InteractionBatchAggregator::Aggregate");
      Assert.ArgumentNotNull(batch, "batch");
      Trace.Info("batch : [Items: {0}; PostponedItems: {1}]".FormatWith(batch.Items.Count, batch.PostponedItems.Count));
      AnalyticsCount.AggregationActiveInteractionAggregators.Value = Interlocked.Increment(ref aggregators);
      this.UpdateAverageBatchSizeCounter(batch.Items.Count);
      try
      {
        ICollection<ReportingStorageItem> is2 = new Collection<ReportingStorageItem>();
        foreach (InteractionKey key in batch.Items)
        {
          Trace.Info("Processing Interaction Key: [ConteactId: {0}; InteractionId: {1}]".FormatWith(key.ContactId.ToString(), key.InteractionId.ToString()));
          Assert.Required(this.Context, "The BatchAggregator.Context property has to be set.");
          Assert.Required(this.Context.Source, "The BatchAggregator.Context.Source property has to be set.");
          try
          {
            Trace.Info("Creating context for interaction by key ...");
            IVisitAggregationContext interaction = this.Context.Source.CreateContextForInteraction(key);
            Trace.Info("Done: [{0}]".FormatWith(interaction == null ? "NULL" : interaction.ToString()));
            if (interaction != null)
            {
              Trace.Info(" > Processing interaction");
              AggregationDataSet set;
              byte[] reportingStorageItemIdentifier = this.GetReportingStorageItemIdentifier(key);
              Trace.Info(" > Fetched storage item idnetifier: [{0}]".FormatWith(reportingStorageItemIdentifier == null ? "NULL" : System.Text.Encoding.ASCII.GetString(reportingStorageItemIdentifier)));
              using (new OperationStopwatch(base.CounterAverageAggregationTime, 0x3e8L))
              {
                Trace.Info(" > Invoking Aggregate on interaction...");
                set = this.Aggregate(interaction);
                Trace.Info(" > Done [{0}]".FormatWith(set == null ? "NULL" : set.ToString()));
              }
              if (!set.IsEmpty)
              {
                Trace.Info(" * Set is not empty, processing");
                ReportingStorageItem item = new ReportingStorageItem(reportingStorageItemIdentifier, set, key);
                Trace.Info(" * Generated reporting storage item: [Id :{0}; ]".FormatWith(System.Text.Encoding.ASCII.GetString(item.Id)));
                Trace.Info(" * Adding reporting storage item to the reporting collection ...");
                is2.Add(item);
                Trace.Info(" * Done");
              }
            }
            else
            {
              Trace.Info(" > Interaction is empty, postponing batch ...");
              batch.PostponeItem(key);
              Trace.Info(" > Done");
            }
          }
          catch (Exception ex)
          {
            Trace.Info("[ERROR] While processing interaction. MSG: {0}".FormatWith(ex.Message));
            Trace.Info(ex.StackTrace);
            Trace.Info("Posponing batch processing ...");
            batch.PostponeItem(key);
            Trace.Info("Done");
          }
        }
        ReportingStorageItemBatch batch2 = new ReportingStorageItemBatch((IEnumerable<IReportingStorageItem>)is2);
        using (new OperationStopwatch(base.CounterAverageWriteTime, 0x3e8L))
        {
          Trace.Info(" ~ storing reporting batch ...");
          this.Target.Store(batch2);
          Trace.Info(" ~ done");
        }
        foreach (ReportingStorageItem item2 in batch2)
        {
          if (batch2.IsPostponed(item2))
          {
            Trace.Info(" - Postponing item [{0}] ...".FormatWith(item2.Id.ToString()));
            batch.PostponeItem(item2.Item);
            Trace.Info(" - done");
          }
          else
          {
            Trace.Info(" + Processing item's dimensions");
            foreach (Dimension dimension in item2.Data.Dimensions)
            {
              Trace.Info(" + > [TableName: {0}]".FormatWith(dimension.TableName));
              //dimension.RegisterKeys();
              //TODO: call the RegisterKeys via reflection
              var dimensionType = dimension.GetType();
              var registerKeysMethod = dimensionType.GetMethod("RegisterKeys", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
              Assert.IsNotNull(registerKeysMethod, "registerKeysMethods NULL");
              registerKeysMethod.Invoke(dimension, new object[] { });
            }
          }
        }
      }
      catch (Exception ex) {
        Trace.Info("[ERROR] invoking interaction batch aggregator. MSG: {0}".FormatWith(ex.Message));
        Trace.Info(ex.StackTrace);
      }
      finally
      {
        AnalyticsCount.AggregationActiveInteractionAggregators.Value = Interlocked.Decrement(ref aggregators);
      }
    }

    private void UpdateAverageBatchSizeCounter(int value)
    {
      AverageCounter counterAverageBatchSize = base.CounterAverageBatchSize;
      if (counterAverageBatchSize != null)
      {
        counterAverageBatchSize.AddMeasurement((long)value, 1L);
      }
    }

  }
}
